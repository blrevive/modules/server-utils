BLRevive module for changing server settings

## features

- override various server settings (see [configuration](#configuration))
    - change the bot name pool
    - disable intermission idle kick
- dumping server information into a file (thanks https://github.com/MajiKau/ !)  (see [server info](#server-information))
    - query informations of a server through http

## runtime prerequisites

- on windows, vcrun 2015 x86 has to be installed (vcrun 140) for Proxy.dll to load correctly, you can find it at https://www.microsoft.com/en-us/download/details.aspx?id=52685
- recent versions of wine should be able to run it without installing msvc, do `winetricks vcrun2015` if that's not the case

## installation

Follow the [module install](https://blrevive.gitlab.io/blrevive/latest/md_documentation__modules.html#installation) in the BLRevive documentation.

## configuration

- the base path (`<ServerConfigs>`) for all server configuration files is `<BLR>/FoxGame/Config/BLRevive/server_utils`.

- the default configuration file is `<ServerConfigs>/default.json`

- the server config file to use can be changed by setting the `blre.server.config` url parameter (eg: `BLRevive.exe server?blre.server.config=awesome_server`)

- if the server config file does not exist a default one is created
- if the server config file contains invalid json the default config will be  used instead

- set config options in the config json file using `<ConfigPath>/<Key>=<Value>`
    - eg: to set the time limit you would have to change `properties/TimeLimit`

- set config options through url parameters using `<UrlPath>.<Key>=<Value>`
    - eg: to set the time limit from url add `?blre.server.props.timelimit=20` to the url parameters

### server properties

- `ConfigPath`: `properties/`
- `UrlPath`: `blre.server.props`

| key | type | description | default |
|---|---|---|---|
| RandomBotNames/[0-18] | string | change one of the 19 possible bot names | - |
| GameRespawnTime | float | seconds until a player can click respawn | 10.0 |
| GameForceRespawnTime | float | seconds until a player is automatically respawned | 30.0 |
| GameSpectatorSwitchDelayTime | float | seconds until a player can switch back from spectator | 120.0 |
| NumEnemyVotesRequiredForKick | int | number of enemy votes required to kick a player | 4 |
| NumFriendlyVotesRequiredForKick | int | number of friendly votes required to kick a player | 2 |
| VoteKickBanSeconds | int | seconds until a kicked player can re-join | 1200 |
| MaxIdleTime | float | seconds until a player is considerd idling and kicked | 180.0 |
| MinRequiredPlayersToStart | int | minimum player required to start a match, setting it to 1 allows starting with a single player | 1 |
| PlayerSearchTime | int | seconds until a round start after properties/MinRequiredPlayersToStart is fulfilled, setting it too low might cause client side issues during map change | 30.0 |
| TimeLimit | int | minutes a round should last, only applies when playlist is used, non playlist should use the launch parameter instead | 10 |
| GoalScore | int | score required to win, could mean very different things in different modes, for example it means the individual score to win in DM, and number of kills in TDM | 3000 |
| KickOnIdleIntermission | bool | kick players who are idel on intermission | false |

### server mutators

- `<ConfigPath>`: `mutators/`
- `<UrlPath>`: `blre.server.mutators`

> All mutators are of boolean type and disabled by default!

| key | description |
|---|---|
| DisableDepots | disaple depot usage (**NOTE**: Only takes effect upon map change) |
| DisableHRV | disable HRV |
| DisableHeadShots | disable headshots |
| StockLoadout | force all loadouts to defaults |
| DisablePrimaries | disable primary weapons |
| DisableSecondaries | disable secondary weapons |
| DisableGear | disable gear |
| DisableTacticalGear | disable tactical gear |
| DisableHealthRegen | disable health regeneration |
| DisableElementalAmmo | disable elemental ammo |
| HeadshotsOnly | make players only vulnerable to headshots |
                                                                                  
## server information

The server-utils module writes information about the current server instance to a json file every *5 seconds*. The info file can be configured with `info/FileName` inside the server config (or using `?blre.server.info.filename=<filename>` url).
Look at [info structure](#info-structure) to see which information is provided.

Additionaly the module provides a builtin http server to serve this information to the web, which can be configured aswell (see [info config](#info-config) below). It is enabled by default and will provide the server info at `http://<Host>:<Port>/<URI>` (eg `http://0.0.0.0:7778/server_info` by default).

### info config

- `<ConfigPath>`: `info/`
- `<UrlPath>`: `blre.server.info`

| key | type | description | default |
|---|---|---|---|
| Serve | bool | enable builtin http info server | true |
| Host | string | the domain/ip of the server this instance is running on (for lan use `0.0.0.0`) | 0.0.0.0 |
| Port | int | the port the http server listens to | 7778 |
| URI | string | the URI of the server info | /server_info |
| FileName | string | the name of the server info file localy | server_info |
| ShowSpectatorTeam | bool | wether to show spectator team in server info | false |

### info structure

```json
{
    "BotCount": 10,
    "GameMode": "TDM",
    "GameModeFullName": "Team Deathmatch",
    "GoalScore": 3000,
    "Map": "helodeck",
    "MaxPlayers": 16,
    "Mutators": {
        "DisableDepots": false,
        "DisableElementalAmmo": false,
        "DisableGear": false,
        "DisableHRV": false,
        "DisableHeadShots": false,
        "DisableHealthRegen": false,
        "DisablePrimaries": false,
        "DisableSecondaries": false,
        "DisableTacticalGear": false,
        "HeadshotsOnly": false,
        "StockLoadout": false
    },
    "PlayerCount": 0,
    "Playlist": "None",
    "RemainingTime": 600,
    "ServerName": "",
    "TeamList": [
        {
            "BotCount": 5,
            "BotList": [
                {
                    "Deaths": 0,
                    "Kills": 0,
                    "Name": "MK3.JO35",
                    "Score": 0
                },
                {
                    "Deaths": 0,
                    "Kills": 0,
                    "Name": "MK1.NA75",
                    "Score": 0
                },
                {
                    "Deaths": 0,
                    "Kills": 0,
                    "Name": "MK2.SH94",
                    "Score": 0
                },
                {
                    "Deaths": 0,
                    "Kills": 0,
                    "Name": "MK3.JG04",
                    "Score": 0
                },
                {
                    "Deaths": 0,
                    "Kills": 0,
                    "Name": "MK3.DS11",
                    "Score": 0
                }
            ],
            "PlayerCount": 0,
            "PlayerList": [],
            "TeamIndex": 0,
            "TeamName": "Team",
            "TeamScore": 0
        },
        {
            "BotCount": 5,
            "BotList": [
                {
                    "Deaths": 0,
                    "Kills": 0,
                    "Name": "MK1.CB67",
                    "Score": 0
                },
                {
                    "Deaths": 0,
                    "Kills": 0,
                    "Name": "MK1.DK05",
                    "Score": 0
                },
                {
                    "Deaths": 0,
                    "Kills": 0,
                    "Name": "MK1.AW15",
                    "Score": 0
                },
                {
                    "Deaths": 0,
                    "Kills": 0,
                    "Name": "MK2.BR63",
                    "Score": 0
                },
                {
                    "Deaths": 0,
                    "Kills": 0,
                    "Name": "MK1.KR82",
                    "Score": 0
                }
            ],
            "PlayerCount": 0,
            "PlayerList": [],
            "TeamIndex": 1,
            "TeamName": "Team",
            "TeamScore": 0
        }
    ],
    "TimeLimit": 600
}
```

## build

Follow the [BLRevive/Module/Build](https://blrevive.gitlab.io/blrevive/latest/md_documentation__modules.html#building) instructions to build this module.