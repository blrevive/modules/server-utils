#include <server-utils/server-info.h>
#include <server-utils/webserver.h>
#include <server-utils/server-utils.h>
#include <BLRevive/Detours/FunctionDetour.h>
#include <SdkHeaders.h>
#include <windows.h>
#include <chrono>
using namespace BLRE;

void ServerInfoProvisioner::Init()
{
    ServerConfig serverConfig = _serverUtils->serverConfig;

	if(serverConfig.webserver.Enable) {
		_serverUtils->WebServer->AddConnectionHandler(Network::RequestType::GET, serverConfig.webserver.InfoURI,
			[this](const httplib::Request& req, httplib::Response& res) {
				this->ServerInfoRequestHandler(req, res);
			});
	}

    auto dtStartMatch = Detours::FunctionDetour::Create("FoxGame.FoxGame.StartMatch", this, &ServerInfoProvisioner::StartMatchCb);
    dtStartMatch->Enable();

    auto dtStartMatchTimer = Detours::FunctionDetour::Create("FoxGame.FoxGame.StartMatchTimer", this, &ServerInfoProvisioner::StartMatchTimerCb);
    dtStartMatchTimer->Enable();

    auto dtEndMatch = Detours::FunctionDetour::Create("FoxGame.FoxGame.EndGame", this, &ServerInfoProvisioner::EndGameCb);
    dtEndMatch->Enable();

    auto dtPostLogin = Detours::FunctionDetour::Create("FoxGame.FoxGame.PostLogin", this, &ServerInfoProvisioner::PostLoginCb);
    dtPostLogin->Enable();

    auto dtLogout = Detours::FunctionDetour::Create("FoxGame.FoxGame.Logout", this, &ServerInfoProvisioner::LogoutCb);
    dtLogout->Enable();
}

void ServerInfoProvisioner::UpdateServerInfo()
{
	// @todo find the reason why game->FGRI->ServerName is truncated when using playlist parameter (#8)
	//info.ServerName = game->FGRI->ServerName.ToChar();
    _info.ServerName = _serverUtils->ServerName;
}

void ServerInfoProvisioner::UpdateTeamInfo(AFoxGame* game)
{
    Log->debug("updating team infos");
    // @todo use FoxTeamInfo.AddToTeam and FoxTeamInfo.RemoveFromTeam to keep track of players/bots
	// use FoxIntermission.BeginTransitionToNewMap to populate team array
	auto teamsInfo = std::vector<TeamInfoStat>();
    int totalPlayerCount = 0;
    int totalBotCount = 0;
	for (auto team : game->Teams)
	{
		if (!_serverUtils->serverConfig.webserver.ShowSpectatorTeam && team->TeamIndex == game->NumTeams - 1)
			continue;

		auto teamInfo = TeamInfoStat();
		teamInfo.TeamScore = team->Score;
		teamInfo.TeamIndex = team->TeamIndex;
		teamInfo.BotCount = team->BotSize;
		teamInfo.PlayerCount = team->HumanSize;
		teamInfo.TeamName = team->TeamName.ToChar();

		for (auto pri : game->GameReplicationInfo->PRIArray)
		{
			if (!pri->Team || team->TeamIndex != pri->Team->TeamIndex)
				continue;

			auto playerInfo = ActorInfoStat();
			playerInfo.Name = pri->PlayerName.ToChar();
			playerInfo.Kills = pri->Kills;
			playerInfo.Deaths = pri->Deaths;
			playerInfo.Score = pri->Score;

			if (pri->bBot)
				teamInfo.BotList.push_back(playerInfo);
			else
				teamInfo.PlayerList.push_back(playerInfo);
		}

		teamsInfo.push_back(teamInfo);
		totalPlayerCount += teamInfo.PlayerCount;
		totalBotCount += teamInfo.BotCount;
	}

    WaitForSingleObject(_infoMutex, INFINITE);
	_info.TeamList = teamsInfo;
    _info.PlayerCount = totalPlayerCount;
    _info.BotCount = totalBotCount;
    ReleaseMutex(_infoMutex);
    Log->info("updated team infos (players={}, bots={})", totalPlayerCount, totalBotCount);
}

void ServerInfoProvisioner::UpdateMutatorInfo(AFoxGame* game)
{
    Log->debug("updating mutator infos");
    ServerMutators mutators = {};
	mutators.DisableDepots = game->FGRI->IsMutatorEnabled(3);
	mutators.DisableHRV = game->FGRI->IsMutatorEnabled(4);
	mutators.DisableHeadShots = game->FGRI->IsMutatorEnabled(6);
	mutators.StockLoadout = game->FGRI->IsMutatorEnabled(7);
	mutators.DisablePrimaries = game->FGRI->IsMutatorEnabled(8);
	mutators.DisableSecondaries = game->FGRI->IsMutatorEnabled(9);
	mutators.DisableGear = game->FGRI->IsMutatorEnabled(10);
	mutators.DisableTacticalGear = game->FGRI->IsMutatorEnabled(11);
	mutators.DisableHealthRegen = game->FGRI->IsMutatorEnabled(12);
	mutators.DisableElementalAmmo = game->FGRI->IsMutatorEnabled(13);
	mutators.HeadshotsOnly = game->FGRI->IsMutatorEnabled(14);
	mutators.StaminaModifier = game->FGRI->StaminaModifier;
	mutators.HealthModifier = game->FGRI->HealthModifier;

    WaitForSingleObject(_infoMutex, INFINITE);
    _info.Mutators = mutators;
    ReleaseMutex(_infoMutex);
    Log->info("updated mutator infos");
}

void ServerInfoProvisioner::UpdateGameInfo(AFoxGame* game)
{
    WaitForSingleObject(_infoMutex, INFINITE);
	_info.Map = game->WorldInfo->GetMapName(true).ToChar();
	_info.Playlist = game->FGRI->playlistName.GetName();
	_info.GameModeFullName = game->GameTypeString.ToChar();
	_info.GameMode = game->GameTypeAbbreviatedName.ToChar();
	_info.GoalScore = game->GoalScore;
	_info.MaxPlayers = game->MaxPlayers;
	_info.TimeLimit = game->FGRI->TimeLimit * 60;

	// @todo either keep here or provide timestamp of game start and calculate on clients
	_info.RemainingTime = game->FGRI->RemainingTime;

	_info.PlayerCount = 0;
	_info.BotCount = 0;
    ReleaseMutex(_infoMutex);
    UpdateMutatorInfo(game);
    UpdateTeamInfo(game);
}

void ServerInfoProvisioner::ServerInfoRequestHandler(const httplib::Request& req, httplib::Response& res)
{
    ServerInfo info = {};
	WaitForSingleObject(_infoMutex, INFINITE);
    info = _info;
	ReleaseMutex(_infoMutex);

    // calculate remaining time
    int currTimestamp = GetUnixTimestamp();
    int timePlayed = lastMatchStartTime == 0 ? 0 : currTimestamp - lastMatchStartTime;
    info.RemainingTime = info.TimeLimit - timePlayed;

	nlohmann::json j = info;
	res.status = 200;
	res.set_content(j.dump(4), "application/json");
}

void DETOUR_CB_CLS_IMPL(ServerInfoProvisioner::StartMatchCb, AFoxGame, StartMatch, game)
{
    detour->Continue();
    this->UpdateGameInfo(game);
}

void DETOUR_CB_CLS_IMPL(ServerInfoProvisioner::StartMatchTimerCb, AFoxGame, StartMatchTimer, game)
{
    detour->Continue();
    lastMatchStartTime = GetUnixTimestamp();
}

void DETOUR_CB_CLS_IMPL(ServerInfoProvisioner::EndGameCb, AFoxGame, EndGame, game)
{
    detour->Continue();
    lastMatchStartTime = 0;
}

void DETOUR_CB_CLS_EVENT_IMPL(ServerInfoProvisioner::PostLoginCb, AFoxGame, PostLogin, game)
{
    detour->Continue();
    this->UpdateTeamInfo(game);
}

void DETOUR_CB_CLS_IMPL(ServerInfoProvisioner::LogoutCb, AFoxGame, Logout, game)
{
    detour->Continue();
    this->UpdateTeamInfo(game);
}

int ServerInfoProvisioner::GetUnixTimestamp()
{
    const auto t = std::chrono::system_clock::now();
    return std::chrono::duration_cast<std::chrono::seconds>(t.time_since_epoch()).count();
}