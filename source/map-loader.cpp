#include <server-utils/map-loader.h>
#include <server-utils/server-utils.h>
#include <magic_enum.hpp>
#include <BLRevive/Detours/FunctionDetour.h>

using namespace BLRE;

MapLoader::TStreamArrayMap MapLoader::MapTypeStreamArray = MapLoader::TStreamArrayMap
{
    { EMapFileType::Pathing, { &UFoxDataProvider_MapInfo::BaseLevelStreamNames }},
    { EMapFileType::Spawns, {
        &UFoxDataProvider_MapInfo::DMStreamNames,
        &UFoxDataProvider_MapInfo::TDMStreamNames
    }},
    { EMapFileType::Audio, { &UFoxDataProvider_MapInfo::BaseLevelStreamNames}},
    { EMapFileType::CTF, { &UFoxDataProvider_MapInfo::CTFStreamNames }},
    { EMapFileType::SND, {&UFoxDataProvider_MapInfo::SNDStreamNames}},
    { EMapFileType::LMS, {&UFoxDataProvider_MapInfo::LMSStreamNames}},
    { EMapFileType::TKOTH, {&UFoxDataProvider_MapInfo::KOTHStreamNames}},
    { EMapFileType::Domination, {&UFoxDataProvider_MapInfo::DomStreamNames}},
    { EMapFileType::PL, {&UFoxDataProvider_MapInfo::PLStreamNames}},
    { EMapFileType::Netwar, {&UFoxDataProvider_MapInfo::EOTSStreamNames}}
};

MapLoader::TGameModeMap MapLoader::SupportedGameModesByMapFile = MapLoader::TGameModeMap
{
    { EMapFileType::Spawns, {"DM", "tdm", "KC", "LTS"}},
    { EMapFileType::CTF, {"CTF"}},
    { EMapFileType::SND, {"SND"}},
    { EMapFileType::LMS, {"LMS"}},
    { EMapFileType::TKOTH, {"TKOTH"}},
    { EMapFileType::Domination, {"CP"}},
    { EMapFileType::PL, {"PL"}},
    { EMapFileType::Netwar, {"EOTS"}}
};

static std::map<std::string, UClass*> GameModeClassMap = {};

static std::string ToLower(std::string s) {
	std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	return s;
}

std::string MapLoader::GetMapDirectoryPath()
{
    return Utils::FS::BlrBasePath() + "FoxGame\\CookedPCConsole\\Maps\\";
}

void MapLoader::Init()
{
    LoadCustomMapFiles();

    GameModeClassMap = {
        {"DM", AFoxGameMP_DM::StaticClass()},
        {"tdm", AFoxGameMP_TDM::StaticClass()},
        {"KC", AFoxGameMP_KC::StaticClass()},
        {"LTS", AFoxGameMP_LTS::StaticClass()},
        {"LMS", AFoxGameMP_LMS::StaticClass()},
        {"CTF", AFoxGameMP_CTF::StaticClass()},
        {"SND", AFoxGameMP_SND::StaticClass()},
        {"Domination", AFoxGameMP_CP::StaticClass()},
        {"PL", AFoxGameMP_PL::StaticClass()},
        {"Netwar", AFoxGameMP_EOTS::StaticClass()}
    };

	auto dtDirectoryExists = Detours::FunctionDetour::Create("FoxGame.FoxDataStore_Playlists.DoesMapDirectoryExist", this, &MapLoader::DoesMapDirectoryExistCb);
	dtDirectoryExists->Enable();
}

void MapLoader::LoadCustomMapFiles() 
{
	auto mapDirectory = MapLoader::GetMapDirectoryPath();

    if(!std::filesystem::exists(mapDirectory)) {
        Log->debug("custom map directory {} does not exist", mapDirectory);
        return;
    }

    Log->debug("loading custom map files from {}", mapDirectory);
	for (const auto& entry : std::filesystem::recursive_directory_iterator(mapDirectory))
	{
		auto filename = ToLower(entry.path().filename().string());
		if (filename.ends_with(MapExtension)) {
			this->_customMapFiles.insert(filename);
		}
	}

	Log->info("loaded {} map files from {}", _customMapFiles.size(), mapDirectory);
	return;
}

UFoxDataProvider_MapInfo* MapLoader::CreateMapInfo(std::string mapName)
{
    Log->debug("creating MapInfo for {}", mapName);

    if(_customMapFiles.size() == 0) {
        Log->warn("failed to create MapInfo for {}: no map files found", mapName);
        return nullptr;
    }

    FString fMapName = FString(mapName.c_str());
    UFoxDataProvider_MapInfo* mapInfo = UObject::CreateInstance<UFoxDataProvider_MapInfo>();

    // parse base map info
    std::string baseMapFile = mapName + MapExtension;
    if(!_customMapFiles.contains(ToLower(baseMapFile))) {
        Log->warn("failed to create MapInfo for {}: base file {} not found", mapName, baseMapFile);
        return nullptr;
    }

    FName mapStreamName = FName(mapName.c_str());
    mapInfo->Name = mapStreamName;
    mapInfo->MapName = fMapName;
    mapInfo->FriendlyName = fMapName;
    mapInfo->Description = fMapName;
    mapInfo->BaseLevelStreamNames.push_back(mapStreamName);

    for(auto mapFileType : magic_enum::enum_entries<EMapFileType>()) {
        std::string mapFileStreamName = fmt::format("{}_{}", mapName, mapFileType.second);
        if(_customMapFiles.contains(ToLower(mapFileStreamName + MapExtension))) {
            Log->debug("found map file {} for {}", mapFileType.second, mapName);
            FName mapFileStreamFName = FName(mapFileStreamName.c_str());

            for(auto streamNamesArray : MapTypeStreamArray[mapFileType.first]) {
                (mapInfo->*streamNamesArray).push_back(mapFileStreamFName);
            }

            for(auto gameMode : SupportedGameModesByMapFile[mapFileType.first]) {
                mapInfo->SupportedGameModes.push_back(gameMode.c_str());
            }
        }
    }

    Log->info("created MapInfo for {}", mapName);
    return mapInfo;
}

std::vector<FGameMapPair> MapLoader::CreateGameMapPairs(UFoxDataProvider_MapInfo* mapInfo, UFoxDataStore_Playlists* dspl)
{
    for(auto gamePair : dspl->GameMapPairs) {
        Log->info("Map: {}; Class: {}; InfoGameType: {}; InfoURL: {}, InfoName: {}", 
            gamePair.MapName.ToChar(), gamePair.GameClass->GetName(), gamePair.GameModeInfo->GameType.GetName(),
            gamePair.GameModeInfo->URL.ToChar(), gamePair.GameModeInfo->FriendlyName.ToChar());
    }

    return std::vector<FGameMapPair>();
}

bool DETOUR_CB_CLS_IMPL(MapLoader::DoesMapDirectoryExistCb, UFoxDataStore_Playlists, DoesMapDirectoryExist, dspl)
{
    Log->debug("checking for map directory: {}", params->MapProviderName.ToChar());
    detour->Continue();
    return true;
}