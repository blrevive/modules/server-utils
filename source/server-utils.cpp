#pragma warning(disable:4244)
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <winsock2.h>
#include <windows.h>
#include <SdkHeaders.h>
#include <server-utils/server-utils.h>
#include <server-utils/webserver.h>
#include <server-utils/server-info.h>
#include <fstream>
#include <filesystem>
#include <BLRevive/Detours/FunctionDetour.h>
#include <regex>

using namespace BLRE;
using namespace BLRE::Detours;

namespace fs = std::filesystem;

using namespace fmt::literals;

void ServerUtils::Init()
{
	Log->info("Initializing ServerUtils");

	CheckDefaultFiles();

	// load module config (from main config file)
	try {
		moduleConfig = blre->Config.Modules["server-utils"].get<ServerUtilsConfig>();
	} catch(const nlohmann::json::exception& e) {
		Log->warn("failed to serialize server-utils config: {}", e.what());
		moduleConfig = ServerUtilsConfig();
	}
	map_url_config(moduleConfig, blre->URL);

	this->ServerName = blre->URL.GetParam("ServerName", "default");
	this->ServerName = std::regex_replace(this->ServerName, std::regex("([^\\\\])(\\+)"), "$1 ");
	this->ServerName = std::regex_replace(this->ServerName, std::regex(R"(\\\+)"), "+");

	// format config file strings
	#define ResolveFilePath(filePath, basePath) \
		filePath = fmt::format(filePath, "ServerName"_a=ServerName); \
		if(fs::path(filePath).is_relative()) filePath = basePath + filePath;
	
	ResolveFilePath(moduleConfig.ConfigFile, moduleConfig.ConfigsPath);
	ResolveFilePath(moduleConfig.RulesFile, moduleConfig.RulesPath);
	ResolveFilePath(moduleConfig.PatchFile, moduleConfig.PatchesPath);
	ResolveFilePath(moduleConfig.InfoFile, moduleConfig.InfoPath);

	// load server config
	serverConfig = LoadConfig(moduleConfig.ConfigFile);
	infoMutex = CreateMutexA(NULL, false, NULL);

	_mapLoader.Init();

	blre->Patcher->LoadPatchConfig(moduleConfig.PatchFile);
	_playlistProvider.Init();

	// initialize loadout validator
	this->LoadoutValidator.BuildItemClassUIDMap();
	this->LoadoutValidator.LoadValidationMapping();
	this->LoadoutValidator.LoadValidationRules();

	_gameSettings.Init();

	auto dtServerSetLoadout = FunctionDetour::Create("FoxGame.FoxPC.ServerSetLoadout", this, &ServerUtils::ValidatePlayerLoadout);
	dtServerSetLoadout->Enable();

	if(serverConfig.webserver.Enable)
	{
		this->WebServer = Network::HTTPServer::Create("0.0.0.0", blre->URL.GetParam("Port", 7777));
		this->WebServer->AddConnectionHandler(Network::RequestType::GET, serverConfig.webserver.PatchesURI,
			[this](const httplib::Request& req, httplib::Response& res) {
				this->ServerPatchesRequestHandler(req, res);
			});
	}

	_serverInfo.Init();
	_serverInfo.UpdateServerInfo();
	this->WebServer->Start();

	// onslaught fix (allow players with zero netid)
	auto dtIsNetIdAllowed = FunctionDetour::Create("FoxGame.FoxGRI_OS.IsNetIdAllowed",
		+[](DETOUR_FUNC_ARGS_IMPL(AFoxGRI_OS, IsNetIdAllowed, gri))
		{
			detour->Continue();
			return true;
		});
	dtIsNetIdAllowed->Enable();

	// Make all bots join the first team
	if (serverConfig.properties.ForceBotsToSingleTeam) {
		auto dtGetTeamIndexToJoinForBot = FunctionDetour::Create("FoxGame.FoxGameMP_TDM.GetTeamIndexToJoinForBot",
			+[](DETOUR_FUNC_ARGS_IMPL(AFoxGameMP_TDM, GetTeamIndexToJoinForBot, game))
			{
				detour->Continue();
				return 0;
			});
		dtGetTeamIndexToJoinForBot->Enable();
	}

	// Allow team switching from smaller to bigger team
	if (serverConfig.properties.AllowUnbalancedTeams) {
		auto dtCheckPendingTeamSwitches = FunctionDetour::Create("FoxGame.FoxGameMP_TDM.CheckPendingTeamSwitches", this, &ServerUtils::DoPendingTeamSwitches);
		dtCheckPendingTeamSwitches->Enable();
	}
}

void ServerUtils::CheckDefaultFiles()
{
	ServerUtilsConfig defaultConfig;

	if (!fs::exists(defaultConfig.BasePath))
		fs::create_directories(defaultConfig.BasePath);
	if (!fs::exists(defaultConfig.PatchesPath))
		fs::create_directories(defaultConfig.PatchesPath);
	if (!fs::exists(defaultConfig.ConfigsPath))
		fs::create_directories(defaultConfig.ConfigsPath);
	if (!fs::exists(defaultConfig.RulesPath))
		fs::create_directories(defaultConfig.RulesPath);
	if (!fs::exists(defaultConfig.PlaylistsPath))
		fs::create_directories(defaultConfig.PlaylistsPath);
	if (!fs::exists(defaultConfig.InfoPath))
		fs::create_directories(defaultConfig.InfoPath);
}

ServerConfig ServerUtils::LoadConfig(std::string configFilePath)
{
	ServerConfig config = {};

	// check if config file exists
	if(!fs::exists(configFilePath))
	{
		Log->warn("server config {} doesn't exist", configFilePath);

		// use default config file if exists; otherwise use builtin default
		std::string defaultConfigFile = moduleConfig.ConfigsPath + "default.json";
		if(!fs::exists(defaultConfigFile)) {
			Log->warn("default server config {} doesn't exist, using builtin default config", defaultConfigFile);
			configFilePath = "";
		} else {
			Log->info("loading default server config from {}", defaultConfigFile);
			configFilePath = defaultConfigFile;
		}
	}

	// parse config file if found
	if(!configFilePath.empty()) {
		try {
			config = Utils::ReadJsonFile<ServerConfig>(configFilePath);
		}
		catch (nlohmann::json::exception e) {
			Log->error("failed parsing {}, using builtin default config", configFilePath);
			Log->error(e.what());
		}
	}

	// map url params to config
	map_url_config(config.mutators, blre->URL);
	map_url_config(config.properties, blre->URL);
	map_url_config(config.webserver, blre->URL);
	return config;
}

void DETOUR_CB_CLS_IMPL(ServerUtils::ValidatePlayerLoadout, AFoxPC, ServerSetLoadout, pc)
{
	detour->Continue();
	this->LoadoutValidator.ValidatePlayerLoadout(pc);
}

bool DETOUR_CB_CLS_IMPL(ServerUtils::DoPendingTeamSwitches, AFoxGameMP_TDM, CheckPendingTeamSwitches, game)
{
	AFoxPC* FirstTeam1PendingChange;
	FirstTeam1PendingChange = game->GetFirstPendingTeamChange(0);
	if (FirstTeam1PendingChange)
	{
		// Increasing the ValidPlayerSize allows all team switches from the now "bigger" team to the other one
		game->Teams[0]->ValidPlayerSize += 1000;
		detour->Continue();
		game->Teams[0]->ValidPlayerSize -= 1000;
	}
	else
	{
		game->Teams[1]->ValidPlayerSize += 1000;
		detour->Continue();
		game->Teams[1]->ValidPlayerSize -= 1000;
	}
	return params->ReturnValue;
}

void ServerUtils::ServerPatchesRequestHandler(const httplib::Request& req, httplib::Response& res)
{
	res.status = 200;
	res.set_content(blre->Patcher->GetPatchConfig().dump(), "application/json");
}

/**
 * Initialization of the module. 
 * 
 * @remark **Do not change the name or remove the __declspec(dllexport) from this function, otherwise BLRevive will fail to load the module!**
 * 
 * @param blre pointer to BLRevive API
*/
extern "C" __declspec(dllexport) void InitializeModule(BLRevive *blre)
{
	if (!Utils::IsServer()) {
		return;
	}

	ServerUtils* serverUtils = new ServerUtils(blre);
	serverUtils->Init();
}

BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved)
{
	return true;
}