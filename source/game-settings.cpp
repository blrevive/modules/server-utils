#include <server-utils/game-settings.h>
#include <SdkHeaders.h>
#include <BLRevive/Detours/FunctionDetour.h>
#include <server-utils/server-utils.h>
#include <magic_enum.hpp>

using namespace BLRE;

static FName CreateName(const char* name)
{
	FName fname;
	fname.Index = FName::Names()->Count;
	FNameEntry* entry = new FNameEntry(name, fname.Index);
	FName::Names()->push_back(entry);
	return fname;
}

static FName FindOrCreateName(const char* name)
{
	for (FNameEntry* nameEntry : *FName::Names()) {
		if (!nameEntry)
			continue;

		if (!stricmp(nameEntry->Name, name))
			return FName(nameEntry->Index());
	}

	return CreateName(name);
}

void GameSettingsProvisioner::Init()
{
    auto dtInitGame = Detours::FunctionDetour::Create("FoxGame.FoxGame.InitGame", this, &GameSettingsProvisioner::InitGameCb);
    dtInitGame->Enable();

	auto dtAIPresetsRegistered = Detours::FunctionDetour::Create("FoxGame.FoxDataStore_AIPresets.Registered", this, &GameSettingsProvisioner::RegisterAIPresets);
	dtAIPresetsRegistered->Enable();

	this->ApplyDefaultSettings();
}

void GameSettingsProvisioner::ApplyDefaultSettings()
{
	Log->debug("applying default settings");
	
	// set default intermission 
	UFoxIntermission* defim = UObject::GetDefaultInstanceOf<UFoxIntermission>();
	defim->PlayerSearchTime  = std::max(_serverUtils->serverConfig.properties.PlayerSearchTime, 0.0f);
	defim->MinRequiredPlayersToStart = std::max(_serverUtils->serverConfig.properties.MinRequiredPlayersToStart, 1);

	// set default game replication info settings
	auto griClasses = UObject::GetInstancesOf<AFoxGRI>();
	for(auto gri : griClasses) {
		if(std::string(gri->GetFullName()).find("Default__") != std::string::npos) {
			gri->MinRequiredToStartGame = defim->MinRequiredPlayersToStart;
		}
	}

	auto games = UObject::GetInstancesOf<AFoxGame>();
	// override random bot names
	if(_serverUtils->serverConfig.properties.RandomBotNames.size() > 0)
	{
		TArray<FString> randomBotNames = {};	
        for(std::string botName : _serverUtils->serverConfig.properties.RandomBotNames)
            randomBotNames.push_back(botName.c_str());
		for(auto game : games)
			game->RandomBotNames = randomBotNames;
	}

	// override bot providers
	auto botProviders = _serverUtils->serverConfig.properties.BotProviders;
	if (botProviders.size() > 0) {
		for (auto game : games) {
			game->BotInfo = {};
			for (int i = 0; i < botProviders.size(); i++) {
				game->BotInfo.push_back({ FindOrCreateName(std::get<0>(botProviders[i]).c_str()) , std::get<1>(botProviders[i]) });
			}
		}
	}

	Log->info("applied default settings");
}

void GameSettingsProvisioner::ApplySettings(AFoxGame* game)
{
    Log->debug("applying game settings for {}", game->GetName());

    // get game properties from playlist/server config
	int currPlaylistIndex = game->PlaylistsDataStore->PlaylistCycleIndex;
	auto playlist = _serverUtils->_playlistProvider.GetPlaylist();

    std::string propertiesProvider = currPlaylistIndex < playlist.size() ?
        fmt::format("Playlist[{}]", currPlaylistIndex) : "ServerConfig";
	const ServerProperties& props = currPlaylistIndex < playlist.size() ?
		playlist[currPlaylistIndex].Properties : _serverUtils->serverConfig.properties;
    Log->debug("using settings from {}", propertiesProvider);

    // game settings
	//game->FGRI->ServerName = _serverUtils->ServerName;
	game->eventGetGameSettings()->SetStringProperty(1073741833, _serverUtils->ServerName);
	game->TimeLimit = game->FGRI->TimeLimit = std::max(props.TimeLimit, 1);
	game->FGRI->RemainingMinute = game->TimeLimit;
	game->FGRI->RemainingTime = game->TimeLimit * 60;
	game->NumBots = game->FGRI->NumBots = std::max(props.NumBots, 0);
	game->MaxBotCount = game->NumBots;
	game->MaxPlayers = game->MaxPlayersAllowed = std::max(props.MaxPlayers, 1);
    game->GameRespawnTime = std::max(props.GameRespawnTime, 0.1f);
	game->GameForceRespawnTime = std::max(props.GameForceRespawnTime, 0.1f);
	game->NumEnemyVotesRequiredForKick = std::max(props.NumEnemyVotesRequiredForKick, 0);
	game->NumFriendlyVotesRequiredForKick = std::max(props.NumFriendlyVotesRequiredForKick, 0);
	game->VoteKickBanSeconds = std::max(props.VoteKickBanSeconds, 0);
	game->MaxIdleTime = std::max(props.MaxIdleTime, 0.0f);
	game->GoalScore = game->FGRI->GoalScore = std::max(props.GoalScore, 1);

    ServerMutators& mutators = _serverUtils->serverConfig.mutators;

	// More mutators in Engine_classes.h line 2382
	game->FGRI->SetMutatorEnabled(3, mutators.DisableDepots);
	game->FGRI->SetMutatorEnabled(4, mutators.DisableHRV);
	game->FGRI->SetMutatorEnabled(6, mutators.DisableHeadShots);
	game->FGRI->SetMutatorEnabled(7, mutators.StockLoadout);
	game->FGRI->SetMutatorEnabled(8, mutators.DisablePrimaries);
	game->FGRI->SetMutatorEnabled(9, mutators.DisableSecondaries);
	game->FGRI->SetMutatorEnabled(10, mutators.DisableGear);
	game->FGRI->SetMutatorEnabled(11, mutators.DisableTacticalGear);
	game->FGRI->SetMutatorEnabled(12, mutators.DisableHealthRegen);
	game->FGRI->SetMutatorEnabled(13, mutators.DisableElementalAmmo);
	game->FGRI->SetMutatorEnabled(14, mutators.HeadshotsOnly);

	// "Custom" mutators
	game->FGRI->StaminaModifier = mutators.StaminaModifier;
	game->FGRI->HealthModifier = mutators.HealthModifier;

    EGameStatus gameStatus = (EGameStatus)game->FGRI->GameStatus;
    Log->info("applied game settings for {} ({}) from {}", game->GetName(), magic_enum::enum_name(gameStatus), propertiesProvider);
}

void DETOUR_CB_CLS_EVENT_IMPL(GameSettingsProvisioner::RegisterAIPresets, UFoxDataStore_AIPresets, Registered, ds)
{
	detour->Continue();

	auto customBots = _serverUtils->serverConfig.properties.CustomBots;

	for (auto bot : customBots) {
		UFoxDataProvider_AIPresets* newBot = UObject::CreateInstance<UFoxDataProvider_AIPresets>();
		newBot->Name = FindOrCreateName(bot.Name.c_str());
		newBot->AIArchetype = bot.AIArchetype;
		newBot->PawnArchetype = bot.PawnArchetype;
		newBot->PlayerName = bot.PlayerName;
		newBot->DialogNames = { };
		for (auto dialog : bot.DialogNames) {
			newBot->DialogNames.push_back(dialog);
		}
		newBot->bRandomName = bot.bRandomName;
		newBot->PrimaryPreset = bot.PrimaryPreset;
		newBot->SecondaryPreset = bot.SecondaryPreset;
		newBot->Gear1Provider = FindOrCreateName(bot.Gear1Provider.c_str());
		newBot->Gear2Provider = FindOrCreateName(bot.Gear2Provider.c_str());
		newBot->TacticalProvider = FindOrCreateName(bot.TacticalProvider.c_str());
		newBot->StartingWeaponType = bot.StartingWeaponType;
		newBot->HelmetProvider = FindOrCreateName(bot.HelmetProvider.c_str());
		newBot->UpperBodyProvider = FindOrCreateName(bot.UpperBodyProvider.c_str());
		newBot->LowerBodyProvider = FindOrCreateName(bot.LowerBodyProvider.c_str());
		newBot->BodyCamoIndex = bot.BodyCamoIndex;
		newBot->bRespawnAfterDeath = bot.bRespawnAfterDeath;
		newBot->bHasDeathAnimation = bot.bHasDeathAnimation;
		newBot->StatId = bot.StatId;
		newBot->AvatarProvider = { };
		for (auto avatar : bot.AvatarProviders) {
			newBot->AvatarProvider.push_back(FindOrCreateName(avatar.c_str()));
		}
		newBot->bSeeThruStealth = bot.bSeeThruStealth;
		ds->AIProviders.Max++;
		ds->AIProviders.push_back(newBot);
	}

}

void DETOUR_CB_CLS_EVENT_IMPL(GameSettingsProvisioner::InitGameCb, AFoxGame, InitGame, game)
{
    Log->info("initializing game settings");

    detour->Continue();
    this->ApplySettings(game);
    _serverUtils->_serverInfo.UpdateGameInfo(game);
}