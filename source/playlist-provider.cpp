#include <server-utils/playlist-provider.h>
#include <server-utils/server-utils.h>
#include <BLRevive/Detours/FunctionDetour.h>
#include <BLRevive/Utils.h>
#include <filesystem>
#include <nlohmann/json.hpp>
#include <BLRevive/BLRevive.h>

using namespace BLRE;
namespace fs = std::filesystem;

const std::set<std::string> DefaultMaps =
{
	"deadlock",
	"piledriver",
	"heavymetal",
	"vertigo",
	"containment",
	"vortex",
	"helodeck",
	"seaport",
	"decay",
	"evac",
	"trench",
	"metro",
	"centre",
	"shelter",
	"safehold",
	"lockdown",
	"deathmetal",
	"terminus",
	"outpost",
	"convoy",
	"rig",
	"crashsite",
	"gunrange"
};

static FName CreateName(const char* name)
{
	FName fname;
	fname.Index = FName::Names()->Count;
	FNameEntry* entry = new FNameEntry(name, fname.Index);
	FName::Names()->push_back(entry);
	return fname;
}

static FName FindOrCreateName(const char* name)
{
	for (FNameEntry* nameEntry : *FName::Names()) {
		if (!nameEntry)
			continue;

		if (!stricmp(nameEntry->Name, name))
			return FName(nameEntry->Index());
	}

	return CreateName(name);
}

static std::string ToLower(std::string s) {
	std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	return s;
}

PlaylistProvider::PlaylistProvider(ServerUtils* serverUtils)
    : Component("PlaylistProvider") 
{
    _serverUtils = serverUtils;
    blre = BLRevive::GetInstance();
}

std::vector<PlaylistEntry> PlaylistProvider::GetPlaylist()
{
	return _playlist;
}

void PlaylistProvider::Init()
{
	// get playlist name from url
	std::string playlistName = blre->URL.GetParam("Playlist", "");
	if (playlistName.empty()) {
		Log->info("using default playlist");
		return;
	}

	// load playlist config
	std::string playlistConfigFile = _serverUtils->moduleConfig.PlaylistsPath + playlistName + ".json";
	if (!fs::exists(playlistConfigFile)) {
		Log->info("custom playlist file {} not found, using default", playlistName);
		return;
	}

	Log->info("loading playlist {}", playlistName);
	nlohmann::json playlist = Utils::ReadJsonFile<std::vector<nlohmann::json>>(playlistConfigFile);

	// merging playlist properties with server properties
	nlohmann::json jServerProps = _serverUtils->serverConfig.properties;
	for (auto& playlistEntry : playlist) {
		nlohmann::json jEntryProps = jServerProps;
		jEntryProps.merge_patch(playlistEntry["Properties"]);
		playlistEntry["Properties"] = jEntryProps;
		_playlist.push_back(playlistEntry);
	}

	// setup registered 
	auto dtPlaylistDSRegistered = BLRE::Detours::FunctionDetour::Create("FoxGame.FoxDataStore_Playlists.Registered", 
		this, &PlaylistProvider::RegisterCustomPlaylist);
	dtPlaylistDSRegistered->Enable();

	auto dtSelectPlaylistByName = BLRE::Detours::FunctionDetour::Create("FoxGame.FoxDataStore_Playlists.SelectPlaylistByName",
		this, &PlaylistProvider::SelectPlaylistByNameCb);
	dtSelectPlaylistByName->Enable();

	auto dtCyclePlaylist = Detours::FunctionDetour::Create("FoxGame.FoxDataStore_Playlists.CyclePlaylist",
		+[](DETOUR_FUNC_ARGS_IMPL(UFoxDataStore_Playlists, CyclePlaylist, pds)) 
		{
			detour->Skip();
			pds->PlaylistCycleIndex++;
			if (pds->PlaylistCycleIndex >= pds->GameMapPairs.Count)
				pds->PlaylistCycleIndex = 0;
			return true;
		});
	dtCyclePlaylist->Enable();
}


bool DETOUR_CB_CLS_EVENT_IMPL(PlaylistProvider::SelectPlaylistByNameCb, UFoxDataStore_Playlists, SelectPlaylistByName, plds)
{
	detour->Continue();
	plds->RandomType = EPlaylistType::PlaylistType_Repeat;

	if (params->playlistName.Index == 0)
		return true;

	FName playlistName(params->playlistName.GetName());
	for (auto provider : plds->PlayListProviders) {
		if (provider->Name == playlistName) {
			provider->Name = params->playlistName;
			plds->CachedPlaylistInfo = provider;
			break;
		}
	}

	if (plds->CachedPlaylistInfo == nullptr) {
		Log->warn("failed to select playlist: cached playlist info is null");
		return false;
	}

	plds->ResetCycleInfo();
	if (!plds->BuildPlaylistInfoByProvider(plds->CachedPlaylistInfo, &plds->MapProviders, &plds->GameProviders, &plds->PlaylistGameProviders)) {
		Log->warn("failed to select playlist: could not build playlist info from provider");
		return false;
	}


	plds->BuildPlaylistCycleInfo(&plds->GameMapPairs);
	plds->LastLoadedPlaylistName = params->playlistName;
	return true;
}


void DETOUR_CB_CLS_EVENT_IMPL(PlaylistProvider::RegisterCustomPlaylist, UFoxDataStore_Playlists, Registered, ds)
{
	detour->Continue();

	Log->info("CHECKING FOR CUSTOM MAPS");
	auto menuItems = ds->MenuItemsDataStore;

	auto newMaps = std::set<std::string>();
	for (auto playlistEntry : this->_playlist) {
		
		if (DefaultMaps.contains(ToLower(playlistEntry.Map))) continue;
		if (newMaps.contains(ToLower(playlistEntry.Map))) continue;

		auto newMapInfo = _serverUtils->_mapLoader.CreateMapInfo(playlistEntry.Map);
		if (newMapInfo)
		{
			Log->info("Adding map provider for: {}", playlistEntry.Map);
			menuItems->MapProviders.push_back(newMapInfo);
		}

		newMaps.insert(ToLower(playlistEntry.Map));
	}
	Log->info("DONE CHECKING FOR CUSTOM MAPS");

	Log->info("MAPPROVIDERS:");
	for (auto mapDataProvider : menuItems->MapProviders) {
		auto mapProvider = (UFoxDataProvider_MapInfo*)mapDataProvider;
		Log->info(mapProvider->GetName());

		// Add gamemodes for OS maps (and gunrange, but that doesn't work)
		if (mapProvider->SupportedGameModes.Count == 1) {
			mapProvider->SupportedGameModes.push_back(FName("DM"));
			mapProvider->SupportedGameModes.push_back(FName("tdm"));
			mapProvider->SupportedGameModes.push_back(FName("KC"));
			//mapProvider->SupportedGameModes.push_back(FName("LTS"));
		}

		for (auto gameMode : mapProvider->SupportedGameModes) {
			Log->trace(" {} ({},{})", gameMode.GetName(), gameMode.Index, gameMode.A);
		}
	}

	std::string playlistName = blre->URL.GetParam("Playlist", "");
	// add playlist entry to provider
	auto playlistProvider = UObject::CreateInstance<UFoxDataProvider_Playlist>();
	playlistProvider->FriendlyName = playlistName.c_str();
	playlistProvider->NetIndex = -1;
	playlistProvider->Name = FindOrCreateName(playlistName.c_str());
	playlistProvider->bRandomCycle = false;
	playlistProvider->RequiredChunks.push_back(23);

	for (auto playlistEntry : this->_playlist) {
		playlistProvider->PlaylistPairings.push_back({ 
			FindOrCreateName(playlistEntry.Map.c_str()), FindOrCreateName(playlistEntry.GameMode.c_str()) });
	}
	playlistProvider->eventInitializeProvider(false);
	ds->PlayListProviders.push_back(playlistProvider);
}
