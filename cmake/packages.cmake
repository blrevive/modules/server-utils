set(BLREVIVE_VERSION "1.0.0-beta.1" CACHE STRING "version of the blrevive library")

CPMAddPackage(
    NAME httplib
    VERSION 0.14.1
    GITHUB_REPOSITORY yhirose/cpp-httplib
    OPTIONS "HTTPLIB_USE_OPENSSL_IF_AVAILABLE off" "HTTPLIB_USE_ZLIB_IF_AVAILABLE off"
)