#pragma once
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <server-utils/config.h>
#include <httplib.h>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <BLRevive/Component.h>
#include <BLRevive/Detours/FunctionDetourImpl.h>

struct ActorInfoStat
{
	std::string Name;
	int Kills;
	int Deaths;
	int Score;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(ActorInfoStat,
	Name, Kills, Deaths, Score);

struct TeamInfoStat
{
	std::vector<ActorInfoStat> PlayerList = {};
	std::vector<ActorInfoStat> BotList = {};

	int PlayerCount = 0;
	int BotCount = 0;
	int TeamScore = 0;
	int TeamIndex = 0;
	std::string TeamName = "";
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(TeamInfoStat,
	PlayerList, BotList, PlayerCount, BotCount, TeamScore, TeamIndex, TeamName);

struct ServerInfo {
	std::vector<TeamInfoStat> TeamList;
	std::set<std::string> MapList;

	int PlayerCount;
	int BotCount;
	std::string Map;
	std::string ServerName;
	std::string GameMode;
	std::string GameModeFullName;
	std::string Playlist;
	int GoalScore;
	int TimeLimit;
	int RemainingTime;
	int MaxPlayers;

	ServerMutators Mutators;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(ServerInfo,
	TeamList, MapList, PlayerCount, BotCount, Map, ServerName, GameMode, GameModeFullName,
	Playlist, GoalScore, TimeLimit, RemainingTime, MaxPlayers, Mutators);

class ServerUtils;
class AFoxGame;
class AFoxGRI;
namespace BLRE
{
	class ServerInfoProvisioner : Component
	{
	public:
		ServerInfoProvisioner(ServerUtils* serverUtils)
			: _serverUtils(serverUtils), Component("ServerInfo") {}
		
		void Init();

		void UpdateServerInfo();
		void UpdateGameInfo(AFoxGame* game);
		void UpdateTeamInfo(AFoxGame* game);

	private:
		ServerUtils* _serverUtils = nullptr;
		ServerInfo _info = {};
		static inline HANDLE _infoMutex = CreateMutexA(NULL, false, NULL);
		std::string _serverName = "";
		int lastMatchStartTime = 0;
	
		/**
		 * @brief handler for web server info request
		 * @param req request
		 * @param res response (server info as json)
		*/
		void ServerInfoRequestHandler(const httplib::Request& req, httplib::Response& res);

		void UpdateMutatorInfo(AFoxGame* game);

		void UpdateGameState(AFoxGRI* gameRep);

		void DETOUR_CB_CLS(StartMatchCb, AFoxGame, StartMatch);
		void DETOUR_CB_CLS_EVENT(PostLoginCb, AFoxGame, PostLogin);
		void DETOUR_CB_CLS(StartMatchTimerCb, AFoxGame, StartMatchTimer);
		void DETOUR_CB_CLS(LogoutCb, AFoxGame, Logout);
		void DETOUR_CB_CLS(EndGameCb, AFoxGame, EndGame);

		int GetUnixTimestamp();
	};
}