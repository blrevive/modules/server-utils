#pragma once
#include <BLRevive/BLRevive.h>
#include <BLRevive/Detours/FunctionDetour.h>

class ServerUtils;

namespace BLRE
{
    /**
     * @brief Override game settings from server-utils config.
     */
    class GameSettingsProvisioner : Component
    {
    public:
        GameSettingsProvisioner(ServerUtils* serverUtils)
            : _serverUtils(serverUtils), Component("GameSettings") {}
        
        /**
         * @brief Initialize provisioner. Sets up detours.
         */
        void Init();
    private:
        ServerUtils* _serverUtils = nullptr;

        /**
         * @brief Apply game settings from config (server-utils or playlist) to the current game instance.
         * @param game current game instance
         */
        void ApplySettings(class ::AFoxGame* game);

        /**
         * @brief Apply default (global) settings from the configs.
         */
        void ApplyDefaultSettings();

        /**
         * @brief Update game information on the GameInfoProvisioner.
         * @param game current game instance
         */
        void UpdateGameInfo(class ::AFoxGame* game);

        /**
         * @brief Detour for FoxGame::AFoxGame::InitGame which handles overriding the game config.
         */
        void DETOUR_CB_CLS_EVENT(InitGameCb, AFoxGame, InitGame);

        /**
         * @brief Detour for FoxGame::FoxDataStore_AIPresets::Registered which handles adding custom AI presets.
         */
        void DETOUR_CB_CLS_EVENT(RegisterAIPresets, UFoxDataStore_AIPresets, Registered);
    };
}