#pragma once

#include <BLRevive/BLRevive.h>
#include <server-utils/webserver.h>
#include <server-utils/config.h>
#include <server-utils/server-info.h>
#include <BLRevive/Detours/FunctionDetour.h>
#include <BLRevive/Components/Patcher.h>
#include <server-utils/loadout-validator.h>
#include <server-utils/game-settings.h>
#include <server-utils/map-loader.h>
#include <server-utils/playlist-provider.h>

// logger instance of this module
static BLRE::LogFactory::TSharedLogger Log;

/**
 * @brief utilities for servers
*/
class ServerUtils : BLRE::Component
{
public:
	ServerUtils(BLRE::BLRevive* blreInstance) 
		: BLRE::Component("ServerUtils")
	{
		blre = blreInstance;
	}

	/**
	 * @brief initialize the server utils
	*/
	void Init();

	/**
	 * @brief load configuration file
	 * @param configFilePath path to config file
	 * @return ServerConfig configuration
	*/
	ServerConfig LoadConfig(std::string configFilePath);
	
	void ServerPatchesRequestHandler(const httplib::Request& req, httplib::Response& res);

	// server configuration
	ServerConfig serverConfig;
	// module configuration
	ServerUtilsConfig moduleConfig;
	std::shared_ptr<BLRE::Network::HTTPServer> WebServer;

	std::string ServerName;

	// loadout validator
	BLRE::LoadoutValidator LoadoutValidator = { this };
	BLRE::GameSettingsProvisioner _gameSettings = { this };
	BLRE::ServerInfoProvisioner _serverInfo = { this };
	BLRE::MapLoader _mapLoader = { this };
	BLRE::PlaylistProvider _playlistProvider = { this };
	// instance of blrevive api
	BLRE::BLRevive* blre;
private:

private:
	// mutex for server infos (necessary since webserver runs in own thread)
	static inline HANDLE infoMutex = NULL;

	void CheckDefaultFiles();

	/**
	 * @brief detour for FoxGame.FoxPC.ServerSetLoadout which will validate the user loadout
	 * @param  detour instance of detour
	 * @param  pc player to validate loadout for
	 * @param  parms parameters
	*/
	void DETOUR_CB_CLS(ValidatePlayerLoadout, AFoxPC, ServerSetLoadout);

	/**
	 * @brief detour for FoxGame.FoxGameMP_TDM.CheckPendingTeamSwitches which will allow all team changes
	 * @param  detour instance of detour
	 * @param  game current game instance
	 * @param  parms parameters
	*/
	bool DETOUR_CB_CLS(DoPendingTeamSwitches, AFoxGameMP_TDM, CheckPendingTeamSwitches);
};