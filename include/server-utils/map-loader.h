#pragma once
#include <BLRevive/Component.h>
#include <string>
#include <list>
#include <map>
#include <set>
#include <SdkHeaders.h>
#include <BLRevive/Detours/FunctionDetourImpl.h>

class ServerUtils;
class UFoxDataStore_Playlists;

namespace BLRE
{
    enum class EMapFileType
    {
        Pathing,
        Spawns,
        Audio,
        CTF,
        Domination,
        SND,
        TKOTH,
        LMS,
        PL,
        Netwar
    };

    class MapLoader : Component
    {
    public:
        using TStreamArrayMap = std::map<EMapFileType, std::list<TArray<FName> UFoxDataProvider_MapInfo::*>>;
        using TGameModeMap = std::map<EMapFileType, std::list<std::string>>;

        inline static const std::string MapExtension = ".fmap";
        static TStreamArrayMap MapTypeStreamArray;
        static TGameModeMap SupportedGameModesByMapFile;

        MapLoader(ServerUtils* serverUtils)
            : Component("MapLoader") {}

        void Init();
        
        void GetCustomMapFiles();
        static std::string GetMapDirectoryPath();
        UFoxDataProvider_MapInfo* CreateMapInfo(std::string mapName);

        std::vector<FGameMapPair> CreateGameMapPairs(UFoxDataProvider_MapInfo* mapInfo, UFoxDataStore_Playlists* dspl);
    private:
        ServerUtils* _serverUtils;
        std::set<std::string> _customMapFiles = {};

        void LoadCustomMapFiles();

        bool DETOUR_CB_CLS(DoesMapDirectoryExistCb, UFoxDataStore_Playlists, DoesMapDirectoryExist);
    };
}