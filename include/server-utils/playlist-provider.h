#pragma once
#include <BLRevive/Component.h>
#include <BLRevive/Detours/FunctionDetourImpl.h>
#include <vector>
#include <server-utils/config.h>

class ServerUtils;

namespace BLRE
{
    class BLRevive;

    class PlaylistProvider : Component
    {
    public:
        PlaylistProvider(ServerUtils* serverUtils);

        void Init();

        std::vector<PlaylistEntry> GetPlaylist();
    private:
        ServerUtils* _serverUtils;
        BLRE::BLRevive* blre;
        std::vector<PlaylistEntry> _playlist = {};
	    
        void DETOUR_CB_CLS_EVENT(RegisterCustomPlaylist, UFoxDataStore_Playlists, Registered);
 
        /**
         * @brief detour for FoxGame.FoxDataStore_Playlists.SelectPlaylistByName to inject custom playlist
         * @param  detour instance of detour
         * @param  plds instance of playlist datastore
         * @param  parms parameters
        */
        bool DETOUR_CB_CLS_EVENT(SelectPlaylistByNameCb, UFoxDataStore_Playlists, SelectPlaylistByName);
    };
}